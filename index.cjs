const express = require('express');
const { Server } = require('socket.io');
const app = express();
const helmet = require('helmet');
const server = require('http').createServer(app);
const cors = require('cors');
const auth = require('./authentication.cjs');
const session = require('express-session');
const appointments = require('./appointments.cjs')
app.use(helmet());
app.use(
  cors({
    origin: 'http://localhost:5173',
    credentials: true,
  })
);
app.use(express.json());

app.use(
  session({
    secret: process.env.COOKIE_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      secure: process.env.ENVIRONMENT === 'production' ? 'true' : 'auto',
      httpOnly: false,
      sameSite: process.env.ENVIRONMENT === 'production' ? 'none' : 'lax',
      expires: 1000 * 60 * 60 * 24 * 7,
    },
  })
);
app.use('/auth', auth);
app.use('/appointments', appointments)
const io = new Server(server, {
  cors: {
    origin: 'http://localhost:5173',
    credentials: true,
  },
});

io.on('connect', (socket) => {});

server.listen(4000, () => {
  console.log('Server listening on port 4000');
});