const express = require("express");
const router = express.Router();
const pool = require("./src/db.cjs");
const { get } = require('./authentication.cjs');
const { ElevatorSharp } = require('@mui/icons-material');

router.route("/create")
    .get(async (req, res) => {
const getAppointments = await pool.query("SELECT dateofappointment FROM appointments WHERE status='approved'")
        if (getAppointments.rowCount > 0) {
            // const getRawData = getAppointments.rows.reduce((acc, curr) => {
            //     acc.push(curr.dateofappointment);
            //     return acc;
            // }, []);
            res.json({ data: getAppointments.rows })
} else {
    res.json('no approved appointments')
}
})
    .post(async (req, res) => {
        const findAppointOwner = await pool.query("SELECT id, firstname, lastname, egn FROM users WHERE id=$1",
            [+req.body.userId]
        );
    
        if (findAppointOwner.rowCount > 0) {
            const potentialAppointment = await pool.query("INSERT INTO appointments(userid, firstname, lastname, egn, dateofappointment, note, status) VALUES ($1, $2, $3, $4, $5, $6, $7)",
                [findAppointOwner.rows[0].id, findAppointOwner.rows[0].firstname, findAppointOwner.rows[0].lastname, findAppointOwner.rows[0].egn, req.body.date, req.body.note, req.body.status]
            );
            res.json({ status: "success", appointment: findAppointOwner.rows[0] });
        } else {
            res.json({ status: "error" });
        }
    });

router.route("/pending")
    .get(async (req, res) => {
    const getAppointments = await pool.query("SELECT * FROM appointments WHERE status='pending'")
    if (getAppointments.rowCount > 0) {
        res.json({data: getAppointments.rows})
    } else {
        res.json('no pending appointments')
    }
    })
    .post(async (req, res) => {
        try {
            const updateStatus = await pool.query("UPDATE appointments SET status='approved' WHERE id=$1",
                [req.body.id]
            )
            res.json({status:"good job"})
        } catch (e) {
            res.json({status: e})
        }
    })
    .delete(async (req, res) => {
        try {
            const deleteAppointment = await pool.query("DELETE FROM appointments WHERE id=$1",
                [req.body.id]
            )
            res.json({status:"good job"})
        } catch (e) {
            res.json({status: e})
        }
    })

router.route("/scheduleAdmin")
    .get(async (req, res) => {
    const getAppointments = await pool.query("SELECT * FROM appointments WHERE status='approved'")
    if (getAppointments.rowCount > 0) {
        res.json({data: getAppointments.rows})
    } else {
        res.json('no approved appointments')
    }
    })
    .put(async (req, res) => {
        const getAppointment = await pool.query("SELECT * FROM appointments WHERE id=$1", [req.body.id])
        if (getAppointment.rowCount > 0) {
            const insertNewData = await pool.query("UPDATE appointments SET firstname=$1, lastname=$2, dateofappointment=$3, note=$4 WHERE id=$5",
                [req.body.firstName, req.body.lastName, req.body.dateofappointment, req.body.note, req.body.id])
            res.json('success')
        } else {
            res.json("error")
        }
    })
    .post(async (req, res) => {
        const findAppointOwner = await pool.query("SELECT id, firstname, lastname, egn FROM users WHERE egn=$1",
        [req.body.egn]
    );

    if (findAppointOwner.rowCount > 0) {
        const potentialAppointment = await pool.query("INSERT INTO appointments(userid, firstname, lastname, egn, dateofappointment, note, status) VALUES ($1, $2, $3, $4, $5, $6, $7)",
            [findAppointOwner.rows[0].id, req.body.firstName, req.body.lastName, req.body.egn, req.body.dateofappointment, req.body.note, req.body.status]
        );
        res.json({ status: "success", appointment: findAppointOwner.rows[0] });
    } else if(findAppointOwner.rowCount === 0){
        const potentialAppointment = await pool.query("INSERT INTO appointments(userid, firstname, lastname, egn, dateofappointment, note, status) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id",
            [`${Math.random().toFixed(3)}`, req.body.firstName, req.body.lastName, req.body.egn, req.body.dateofappointment, req.body.note, req.body.status]
        );
        res.json({ status: "success", id: potentialAppointment.rows[0].id });
    } else {
        res.json({ status: "error" });
    }
    })
    .delete(async (req, res) => {
        const findAppointment = await pool.query("SELECT * FROM appointments WHERE id=$1",
        [+req.body.id]
        );
        if (findAppointment.rowCount > 0) {
            try {
                const deleteAppointment = await pool.query("DELETE FROM appointments WHERE id=$1",
                    [+req.body.id]
                )
                res.json({status: "success"})
            } catch (e) {
                res.json({status: "error"})
            }
        } else {
            res.json({status: "error"})
        }
})

router.route("/scheduleUser")
.post(async (req, res) => {
    const getAppointments = await pool.query("SELECT * FROM appointments WHERE status='approved' AND userid=$1",[req.body.id])
    if (getAppointments.rowCount > 0) {
        res.json({data: getAppointments.rows})
    } else {
        res.json('no approved appointments')
    }
    })
module.exports = router;
