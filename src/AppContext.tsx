import { createContext } from "react";


interface UserData {
  loggedIn: boolean;
  email: string;
  firstName: string;
  lastName: string;
  egn: string;
}

interface AppContextType {
  userData: UserData;
  setUserData: React.Dispatch<React.SetStateAction<UserData>>;
}

export const AppContext = createContext<AppContextType>({
  userData: {
    loggedIn: false,
    email: "",
    firstName: "",
    lastName: "",
    egn: "",
  },
  setUserData: () => {}, 
});