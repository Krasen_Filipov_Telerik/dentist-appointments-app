CREATE TABLE appointments(
    id SERIAL PRIMARY KEY,
    userID VARCHAR NOT NULL,
    firstName VARCHAR(30),
    lastName VARCHAR(30),
    egn VARCHAR(10),
    dateOfAppointment VARCHAR NOT NULL,
    note VARCHAR(120)
)