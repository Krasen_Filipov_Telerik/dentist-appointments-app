import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import NavBar from '../NavBar/NavBar';
import { useState } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import { useUserContext } from '../../AppInitializers.tsx';
import { useNavigate } from 'react-router-dom';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
function Copyright(props: any) {
  return (
    <Typography
      variant='body2'
      color='text.secondary'
      align='center'
      {...props}
    >
      {'Copyright © '}
      <Link color='inherit' href='https://mui.com/'>
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function Login() {
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [open, setOpen] = useState(false);
  const { setUserData } = useUserContext();
  const navigate = useNavigate();

  const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
  ) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props}  />;
  });



  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    setSpinner(true);
    event.preventDefault();
    const rawData = new FormData(event.currentTarget);
    const data = {
      email: `${rawData.get('email')}`,
      password: `${rawData.get('password')}`,
    };
    if (data.email.trim().length === 0) {
      setEmailError(true);
      setSpinner(false);
      return;
    }
    if (data.password.trim().length === 0) {
      setPasswordError(true);
      setSpinner(false);
      return;
    }
    setEmailError(false);
    setPasswordError(false);
    fetch('http://localhost:4000/auth/login', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Access-Control-Allow-Credentials': 'true',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .catch((err) => {
        console.log(err);
        return;
      })
      .then((res) => {
        if (!res || !res.ok || res.status >= 400) {
          console.log(res);
          return;
        }
        return res.json();
      })
      .then(data => {
        if (!data.userInfo.loggedIn) {
          setUserData({ loggedIn: false, email: "", firstName: "", lastName: "", egn: "" });
          setOpen(true)
          return;
        } else {
            navigate("/")
          localStorage.setItem('currentUserID', data.userInfo.id)
          console.log(data)
        }
      })
    setSpinner(false);
  };

  return (
    <Box>
      <NavBar></NavBar>
      <Snackbar anchorOrigin={{ vertical:"top", horizontal:"center" }} open={open} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error" sx={{ width: '100%'}}>
          Грешен имейл или парола!
        </Alert>
      </Snackbar>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component='h1' variant='h5'>
            Влез
          </Typography>
          <Box
            component='form'
            onSubmit={handleSubmit}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              error={emailError ? true : false}
              helperText={emailError ? 'Грешен или невалиден имейл' : ''}
              margin='normal'
              required
              fullWidth
              id='email'
              label='Имейл'
              name='email'
              autoComplete='email'
              autoFocus
            />
            <TextField
              error={passwordError ? true : false}
              helperText={passwordError ? 'Грешна парола' : ''}
              margin='normal'
              required
              fullWidth
              name='password'
              label='Парола'
              type='password'
              id='password'
              autoComplete='current-password'
            />
            <FormControlLabel
              control={<Checkbox value='remember' color='primary' />}
              label='Запомни ме'
            />
            <Button
              type='submit'
              fullWidth
              variant='contained'
              sx={{ mt: 3, mb: 2 }}
            >
              Влез
            </Button>
            <Grid container>
              <Grid item xs>
                <Link variant='body2'>Забравена парола?</Link>
              </Grid>
              <Grid item>
                <Link variant='body2'>{'Нямаш профил? Регистрация'}</Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        {spinner && (
          <Box sx={{ display: 'flex', justifyContent: 'center', mt: '25px' }}>
            <CircularProgress />
          </Box>
        )}
        <Copyright sx={{ mt: 8, mb: 4 }} />
      </Container>
    </Box>
  );
}
