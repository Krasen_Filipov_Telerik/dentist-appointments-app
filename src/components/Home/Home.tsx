import NavBar from '../NavBar/NavBar'
import { Box, Grid } from '@mui/material'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import profilePicture from '../../../public/DSC_0009(1).jpg'
import { Link } from '@mui/material';
import Divider from '@mui/material/Divider';
import CheckIcon from '@mui/icons-material/Check';
import { Container } from '@mui/material';

const Home = () => {
    
    return (
        <Box>
            <NavBar></NavBar>
            <Container sx={{display:'flex', flexDirection:'row',width:'100%'}}>
            <Card sx={{ width: '100%', mt: '3%'}}>
      <CardMedia
        sx={{ height: 440 }}
        image={profilePicture}
        title="Д-р Георги Димитров Господинов"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
        Д-р Георги Димитров Господинов
        </Typography>
        <Typography variant="body2" color="text.secondary">
        Роден съм на 31.12 1973 г. в град Варна. През 1991г. завърших  ПМГ "Акад. Методий Попов" гр. Варна; през 1995 г., ПМИ гр. Варна бакалавър по зъботехника, през 2002 г.,  МУ гр. Пловдив – магистър по стоматология.
В периода 2003 - 2009 г. работих в дентален кабинет в  гр.Варна – завод Дружба , в с. Страшимирово, с. Разделна и в дентален кабинет към Солвей Соди.
От 2005 г. и по настоящем  имам дентална практика в гр.Варна с адрес: ул. "Хаджи Димитър" 16 ет.1 ап.23
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="large">Share</Button>
        <Button size="large"><Link href="https://www.rating.hapche.bg/rating/lekari-po-dentalna-meditsina/5874/georgi_dimitrov_gospodinov#komentari" target='_blank'>Отзиви</Link></Button>
      </CardActions>
                </Card>
                <Box sx={{width:'55%',mt: '3%', ml: '5%', borderStyle: 'groove',display:'flex',flexDirection:'column',fontSize:'large',height:'fit-content' }}>
                    <Box sx={{ fontWeight: 'bold', mt:'10px', fontSize:'30px', textAlign:'center'}}>Принципите определящи моята работа са:</Box>
      <Box sx={{fontSize:'23px', mt:'10px', textAlign:'center'}}>реализъм  във всичко; <CheckIcon sx={{color:'green'}}></CheckIcon></Box>
                    <Divider></Divider>
                    <Box sx={{fontSize:'23px', mt:'10px', textAlign:'center'}}>реални проблеми; <CheckIcon sx={{color:'green'}}></CheckIcon></Box>
                    <Divider></Divider>
                    <Box sx={{fontSize:'23px', mt:'10px', textAlign:'center'}}>реални решения; <CheckIcon sx={{color:'green'}}></CheckIcon></Box>
                    <Divider></Divider>
                    <Box sx={{fontSize:'23px', mt:'10px', textAlign:'center'}}>реално качество; <CheckIcon sx={{color:'green'}}></CheckIcon></Box>
      <Divider>
                    </Divider>
                    <Box sx={{ fontSize: '23px', mt:'10px', textAlign:'center' }}>реални цени; <CheckIcon sx={{color:'green'}}></CheckIcon></Box>
                </Box>
            </Container>
            <Box sx={{ mt:'25px',display: 'flex', flexDirection: 'column', textAlign: 'center' }}>
            <Divider></Divider>
                    <Box sx={{ fontSize: '23px', fontWeight: 'bold' }}>Доверието не се  купува, с работата си се надявам да спечеля вашето!</Box>
                <Box textAlign='center'>С Уважение: д-р Господинов</Box>
                <Divider></Divider>
        </Box>
            </Box>
    )
}

export default Home
