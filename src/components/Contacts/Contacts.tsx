import NavBar from '../NavBar/NavBar'
import { Box, Link, Divider } from '@mui/material'
import sattelite from '../../../public/mapSattelite.jpg'
import onSite from '../../../public/mapLocation.jpg'
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import HomeIcon from '@mui/icons-material/Home';
import './Images.css'
const Contacts = () => {
    return (
        <Box>
            <NavBar></NavBar>
            <Box sx={{ fontSize: 40, display: 'flex', justifyContent: 'center', mt:'3%' }}><AlternateEmailIcon sx={{ fontSize: 50, alignSelf: 'center' }}></AlternateEmailIcon>Имейл: grimor@abv.bg</Box>
            <Divider></Divider>
            <Box sx={{ fontSize: 40, display: 'flex', justifyContent: 'center'}}><LocalPhoneIcon sx={{ fontSize: 50, alignSelf: 'center' }}></LocalPhoneIcon> Телефон: 052/ 605791; 0898 4444 62</Box>
            <Divider></Divider>
            <Box sx={{ fontSize: 40, display: 'flex', justifyContent: 'center' }}><HomeIcon sx={{ fontSize: 50, alignSelf: 'center' }}></HomeIcon>Адрес: гр.Варна, ул."Хаджи Димитър" №16, етаж 1, ап. 23</Box>
            <Divider></Divider>
            <Box sx={{ display: 'flex', flexDirection: 'row', gap: '30px', mt: '25px', flexWrap: 'wrap', ml:'1%'}} >
                
                <Box>
                    <Link target='_blank' href='https://www.google.com/maps/place/%D0%94%D0%B5%D0%BD%D1%82%D0%B0%D0%BB%D0%BD%D0%B0+%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D0%BA%D0%B0+%D0%B4%D0%BE%D0%BA%D1%82%D0%BE%D1%80+%D0%93%D0%BE%D1%81%D0%BF%D0%BE%D0%B4%D0%B8%D0%BD%D0%BE%D0%B2/@43.2143475,27.9110399,19z/data=!4m6!3m5!1s0x40a4547179a709c9:0xd7431a933b1b2b78!8m2!3d43.2143678!4d27.9109768!16s%2Fg%2F11hyt1gknw?entry=ttu'><img src={sattelite} style={{ width: '925px', height: '600px' }}></img></Link>
                    </Box>
                <Box>
                    <Link target='_blank' href='https://www.google.com/maps/@43.2144781,27.9108511,3a,74.1y,141.62h,86.15t/data=!3m7!1e1!3m5!1sPhNYlo7uwfhERqsN98PP3w!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3DPhNYlo7uwfhERqsN98PP3w%26cb_client%3Dsearch.gws-prod.gps%26w%3D86%26h%3D86%26yaw%3D139.69536%26pitch%3D0%26thumbfov%3D100!7i16384!8i8192?entry=ttu'><img src={onSite} style={{ width: '925px', height: '600px' }}></img></Link>
                </Box>
                </Box>
        </Box>
    )
}
export default Contacts