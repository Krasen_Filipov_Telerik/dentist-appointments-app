import * as React from 'react';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import GitHubIcon from '@mui/icons-material/GitHub';
import LocalPhoneOutlinedIcon from '@mui/icons-material/LocalPhoneOutlined';
import { useState } from 'react';
import footerImage from '../../../public/footer.jpg'
import { Box, Link, Typography } from '@mui/material';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import CheckIcon from '@mui/icons-material/Check';
const Footer = () => {
  const [value, setValue] = React.useState('recents');
    const [open, setOpen] = useState(true);
    const handleClose = (_event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };
  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
  };

  const footerStyle = {
    backgroundImage: `url(${footerImage})`,
    backgroundSize: "cover",
  };

  const containerStyle = {
    position: 'fixed',
    bottom: 0,
    width: '100%',
    zIndex: 1000,
  };

  const copyToClipboard = (text: string) => {
    navigator.clipboard.writeText(text)
    setOpen(true)
    };
    
    const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
        props,
        ref,
      ) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props}  />;
      });

  return (
      <Box sx={containerStyle}>
          <Snackbar anchorOrigin={{ vertical:"bottom", horizontal:"left" }} open={open} autoHideDuration={1000} onClose={handleClose}>
              <Alert onClose={handleClose} severity="info" sx={{ width: '100%', display:'flex', flexDirection:'row' }}>
      <Typography sx={{display:'flex', alignItems:'center', textAlign:'center', fontSize:'16px', justifyContent:'center'}}><CheckIcon></CheckIcon>Копирано</Typography>
        </Alert>
      </Snackbar>
      <BottomNavigation sx={footerStyle} value={value} onChange={handleChange}>
        <Typography textAlign="center" alignSelf='center'>Изработено от: Красен Филипов</Typography>
        <Link target="_blank" href="https://www.linkedin.com/in/krasen-filipov-0bb460289/" sx={{ alignSelf: 'center' }}>
          <BottomNavigationAction value="linkedin" icon={<LinkedInIcon />} />
        </Link>
        <Link target="_blank" href="https://gitlab.com/Krasen_Filipov_Telerik" sx={{ alignSelf: 'center', ml: '40px' }}>
          <BottomNavigationAction value="Git" icon={<GitHubIcon />} />
        </Link>
        <BottomNavigationAction
          label="+359 89 680 5234"
          value="+359 89 680 5234"
          icon={<LocalPhoneOutlinedIcon />}
          sx={{
            ".css-imwso6-MuiBottomNavigationAction-label.Mui-selected": {
              color: 'white'
            },
          }}
          onClick={() => copyToClipboard("+359 89 680 5234")}
        />
        <BottomNavigationAction label="krasenfilipov98@gmail.com"
          value="krasenfilipov98@gmail.com"
          sx={{
            ".css-imwso6-MuiBottomNavigationAction-label.Mui-selected": {
              color: 'white'
            },
            maxWidth: '120px'
          }}
          icon={<EmailOutlinedIcon />}
          onClick={() => copyToClipboard("krasenfilipov98@gmail.com")}
        />
      </BottomNavigation>
    </Box>
  );
};

export default Footer;