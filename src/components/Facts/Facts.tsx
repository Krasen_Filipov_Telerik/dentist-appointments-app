import NavBar from '../NavBar/NavBar'
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import Divider from '@mui/material/Divider';
const Facts = () => {
    return (
        <Box>
            <NavBar></NavBar>
            <Box sx={{display:'flex', flexDirection:'row', flexWrap:'wrap', gap:'30px', ml:'3%', mt:'6%'}}>
            <Card sx={{ maxWidth:'45%'}}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
          Твърдение :  ’’Зъбния камък не се чисти защото машинките с които се
                       прави развалят зъбите’’’
                        </Typography>
                        <Divider></Divider>
                        <Typography gutterBottom variant="h6" sx={{ display: 'flex', justifyContent: 'center', gap: '5px', fontsize:'9px', mt:'5px' }}>Истината:<Typography variant="body1" sx={{display:'flex', color: 'red', alignSelf:'center' }}>Невярно</Typography></Typography>
                        <Divider></Divider>
          <Typography variant="body2" color="text.secondary">
          Зъбния камък уврежда както зъбите и така наричания зъбодържащ апарат- милиони нишки които крепят зъбите в костта. Той бавно и почти безсимптомно избутва венеца, стапя подлежащата кост и води до ранното разклащане и падане на видимо здрави /неувредени от кариес /зъби.
          </Typography>
        </CardContent>
      </CardActionArea>
            </Card>
            <Card sx={{ maxWidth:'45%'}}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
          Твърдение: „ Много боли, много е разрушен, искам да се извади”
                        </Typography>
                        <Divider></Divider>
                        <Typography gutterBottom variant="h6" sx={{ textAlign: 'center', display: 'flex', justifyContent: 'center', gap: '5px', fontsize:'9px', mt:'5px' }}>Истината:<Typography variant="body1" sx={{color: 'red', alignSelf:'center'}}>Неправилно</Typography></Typography>
                        <Divider></Divider>
          <Typography variant="body2" color="text.secondary">
          Стоматологията е напреднала достатъчно за да се справя с болшинството болящи зъби.
Решението за екстракция подлежи само на лекуващия зъболекар след като е преценил и направил възможното за лечението му.
          </Typography>
        </CardContent>
      </CardActionArea>
            </Card>
            <Card sx={{ maxWidth:'45%'}}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
          Твърдение:  ”Предните ми зъби са важни, задните - карай, няма
                       значение."
                        </Typography>
                        <Divider></Divider>
                        <Typography gutterBottom variant="h6" sx={{ textAlign: 'center', display: 'flex', justifyContent: 'center', gap: '5px', fontsize:'9px', mt:'5px' }}>Истината:<Typography variant="body1" sx={{color: 'red', alignSelf:'center'}}>Невярно</Typography></Typography>
                        <Divider></Divider>
          <Typography variant="body2" color="text.secondary">
          Предните зъби имат свойте функци както дъвкателни ,фонетични и естетични , но сами по себе си не оформят пълноценно съзъбие, устойчивостта им е намалена и това се обусклавя и от структората и формата им. Те са еднокоренови и са разположени в по-тънки костни структури. Захапката се определя от задните/дъвкателни /зъби.
При липсата им, частична или пълна, настъпват промени които унищожават и предните които са натоварени с пъти повече от нормалното.
          </Typography>
        </CardContent>
      </CardActionArea>
            </Card>
            <Card sx={{ maxWidth:'45%'}}>
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
          Твърдение: ”Млечните зъби се сменят, защо да се лекуват така или
                      иначе ще паднат”
                        </Typography>
                        <Divider></Divider>
                        <Typography gutterBottom variant="h6" sx={{ textAlign: 'center', display: 'flex', justifyContent: 'center', gap: '5px', fontsize:'9px', mt:'5px' }}>Истината:<Typography variant="body1" sx={{color: 'red', alignSelf:'center'}}>Невярно</Typography></Typography>
                        <Divider></Divider>
          <Typography variant="body2" color="text.secondary">
          Природата е съвършен механизъм, в него излишни части няма!!! При занемаряване на млечното съзъбие настъпват проблеми както с комфорта на детето /храни се само от страната която не го боли/, така и с неправилна обработка на храната.Оставяне на нелекувани кариозни млечни зъби, води до увреждане на зародишите или на самите постоянни зъби.
          </Typography>
        </CardContent>
      </CardActionArea>
                </Card>
                </Box>
        </Box>
    )
}
export default Facts