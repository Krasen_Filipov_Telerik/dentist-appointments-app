import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import NavBar from '../NavBar/NavBar';
import { useState } from 'react';
import {
  MIN_NAME_LENGTH,
  MAX_NAME_LENGTH,
  MIN_PASSWORD_LENGTH,
  MAX_PASSWORD_LENGTH,
  MIN_LAST_NAME_LENGTH,
  MAX_LAST_NAME_LENGTH,
  MIN_EGN_LENGTH,
} from '../../constants.js';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import { useUserContext } from '../../AppInitializers.tsx';
import CircularProgress from '@mui/material/CircularProgress';
import { useNavigate } from 'react-router-dom';
function Copyright(props: any) {
  return (
    <Typography
      variant='body2'
      color='text.secondary'
      align='center'
      {...props}
    >
      {'Copyright © '}
      <Link color='inherit' href='https://mui.com/'>
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function Register() {
  const [errorPassword, setErrorPassword] = useState(false);
  const [errorEmail, setErrorEmail] = useState(false);
  const [errorFirstName, setErrorFirstName] = useState(false);
  const [errorLastName, setErrorLastName] = useState(false);
  const [errorEgn, setErrorEgn] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [open, setOpen] = useState(false);
  const { setUserData } = useUserContext();
  const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
  ) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props}  />;
  });



  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };


  const navigate = useNavigate();
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setSpinner(true)
    const rawData = new FormData(event.currentTarget);
    const data = {
      email: `${rawData.get('email')}`,
      firstName: `${rawData.get('firstName')}`,
      password: `${rawData.get('password')}`,
      lastName: `${rawData.get('lastName')}`,
      egn: `${rawData.get('egn')}`,
    };
    const regex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (data.email.trim().length === 0 || !data.email.match(regex)) {
      setErrorEmail(true);
      setSpinner(false)
      return;
    }
    if (
      data.firstName.trim().length < MIN_NAME_LENGTH ||
      data.firstName.trim().length > MAX_NAME_LENGTH
    ) {
      setErrorFirstName(true);
      setSpinner(false)
      return;
    }
    if (
      data.lastName.trim().length < MIN_LAST_NAME_LENGTH ||
      data.lastName.trim().length > MAX_LAST_NAME_LENGTH
    ) {
      setErrorLastName(true);
      setSpinner(false)
      return;
    }
    if (
      data.password.trim().length < MIN_PASSWORD_LENGTH ||
      data.password.trim().length > MAX_PASSWORD_LENGTH
    ) {
      setErrorPassword(true);
      setSpinner(false)
      return;
    }
    if (
      !(data.egn.trim().length === MIN_EGN_LENGTH) ||
      isNaN(+data.egn.trim())
    ) {
      setErrorEgn(true);
      setSpinner(false)
      return;
    }
    setErrorEgn(false);
    setErrorPassword(false);
    setErrorLastName(false);
    setErrorFirstName(false);
    setErrorEmail(false);
    fetch('http://localhost:4000/auth/register', {
      method: 'POST',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .catch((err) => {
        return;
      })
      .then((res) => {
        if (!res || !res.ok || res.status >= 400) {
          setSpinner(false)
          return;
        }
        return res.json();
      })
      .then(data => {
        if (!data.loggedIn) {
          setUserData({ loggedIn: false, email: "", firstName: "", lastName: "", egn: "" });
          setOpen(true)
          setSpinner(false)
          return;
        } else {
            navigate("/")
            localStorage.setItem('currentUserID', data.userInfo.id)
            localStorage.setItem('currentUserName', data.userInfo.firstName)
            localStorage.setItem('currentUserStatus', data.userInfo.loggedIn)
          setSpinner(false)
          console.log(data)
        }
      })
  };

  return (
    <Box>
      <NavBar></NavBar>
      <Snackbar anchorOrigin={{ vertical:"top", horizontal:"center" }} open={open} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error" sx={{ width: '100%'}}>
          Неуспешно създаване на регистрация!
        </Alert>
      </Snackbar>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component='h1' variant='h5'>
            Регистрация
          </Typography>
          <Box
            component='form'
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  error={errorFirstName ? true : false}
                  helperText={
                    errorFirstName
                      ? 'Името може да бъде между 2 и 20 символа'
                      : ''
                  }
                  autoComplete='given-name'
                  name='firstName'
                  required
                  fullWidth
                  id='firstName'
                  label='Име'
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  error={errorLastName ? true : false}
                  helperText={
                    errorLastName
                      ? 'Фамилията може да бъде между 4 и 20 символа'
                      : ''
                  }
                  required
                  fullWidth
                  id='lastName'
                  label='Фамилия'
                  name='lastName'
                  autoComplete='family-name'
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  error={errorEgn ? true : false}
                  helperText={errorEgn ? 'Навалидно ЕГН' : ''}
                  required
                  fullWidth
                  id='egn'
                  label='ЕГН'
                  name='egn'
                  autoComplete='egn'
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  error={errorEmail ? true : false}
                  helperText={errorEmail ? 'Навалиден Имейл' : ''}
                  required
                  fullWidth
                  id='email'
                  label='Email Address'
                  name='email'
                  autoComplete='email'
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  error={errorPassword ? true : false}
                  helperText={
                    errorPassword
                      ? 'Паролата трябва да бъде между 6 и 20 символа'
                      : ''
                  }
                  required
                  fullWidth
                  name='password'
                  label='Парола'
                  type='password'
                  id='password'
                  autoComplete='new-password'
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox value='allowExtraEmails' color='primary' />
                  }
                  label='I want to receive inspiration, marketing promotions and updates via email.'
                />
              </Grid>
            </Grid>
            <Button
              type='submit'
              fullWidth
              variant='contained'
              sx={{ mt: 3, mb: 2 }}
            >
              Регистрация
            </Button>
            <Grid container justifyContent='flex-end'>
              <Grid item>
                <Link href='#' variant='body2'>
                  Вече имаш профил? Вход
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
      {spinner && (
          <Box sx={{ display: 'flex', justifyContent: 'center', mt: '25px' }}>
            <CircularProgress />
          </Box>
        )}
    </Box>
  );
}
