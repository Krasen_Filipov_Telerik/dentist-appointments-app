import { Button, Box } from '@mui/material';
import { NavLink } from 'react-router-dom';
import BasicMenu from '../BasicMenu/BasicMenu';
import footerImage from '../../../public/footer.jpg'
const NavBar = () => {
  return (
    <Box
      sx={{
        backgroundImage: `url(${footerImage})`,
        backgroundSize: "cover",
        display: 'flex',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        flexWrap: 'wrap',
      }}
    >
      <NavLink to='/'>
        <Button
          sx={{
            '&:hover': {
              backgroundColor: '#90caf9',
              color: 'white',
            },
            color: '#bbdefb',
          }}
        >
          За мен
        </Button>
      </NavLink>
      <NavLink to='/gallery'>
        <Button
          sx={{
            '&:hover': {
              backgroundColor: '#90caf9',
              color: 'white',
            },
            color: '#bbdefb',
          }}
        >
          Галерия
        </Button>
      </NavLink>
      <NavLink to='/contacts'>
        <Button
          sx={{
            '&:hover': {
              backgroundColor: '#90caf9',
              color: 'white',
            },
            color: '#bbdefb',
          }}
        >
          Контакти
        </Button>
      </NavLink>
      <NavLink to='/facts'>
        <Button
          sx={{
            '&:hover': {
              backgroundColor: '#90caf9',
              color: 'white',
            },
            color: '#bbdefb',
          }}
        >
          Вярно ли е?
        </Button>
      </NavLink>
      {localStorage.getItem("currentUserStatus") && !localStorage.getItem("isAdmin") ? <NavLink to='/appointments/create'>
        <Button
          sx={{
            '&:hover': {
              backgroundColor: '#90caf9',
              color: 'white',
            },
            color: '#bbdefb',
          }}
        >
          Запази час
        </Button>
      </NavLink> :
      <NavLink to='/appointments/pending'>
      <Button
        sx={{
          '&:hover': {
            backgroundColor: '#90caf9',
            color: 'white',
          },
          color: '#bbdefb',
        }}
      >
        Заявки
      </Button>
    </NavLink>
      }
      {localStorage.getItem("currentUserStatus") && !localStorage.getItem("isAdmin") ?
        <NavLink to='/appointments/scheduleUser'>
          <Button
            sx={{
              '&:hover': {
                backgroundColor: '#90caf9',
                color: 'white',
              },
              color: '#bbdefb',
            }}
          >
            Мойте часове
          </Button>
        </NavLink>
        : <NavLink to='/appointments/scheduleAdmin'>
          <Button
            sx={{
              '&:hover': {
                backgroundColor: '#90caf9',
                color: 'white',
              },
              color: '#bbdefb',
            }}
          >
            График
          </Button>
        </NavLink>}
      <div>
        <BasicMenu></BasicMenu>
      </div>

    </Box>
  );
};

export default NavBar;
