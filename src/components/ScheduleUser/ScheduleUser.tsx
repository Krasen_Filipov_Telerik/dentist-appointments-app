import * as React from 'react';
import NavBar from '../NavBar/NavBar.tsx'
import { Box, Typography } from '@mui/material'
import { Scheduler } from "@aldabil/react-scheduler";
import { useState, useEffect } from 'react';
interface Appointment {
    id: number;
    userid: string;
    firstname: string;
    lastname: string;
    egn: string;
    dateofappointment: string; 
    note: string;
    status: string;
}
const ScheduleUser = () => {
    const [appointments, setAppointments] = useState<Appointment[]>([])
    useEffect(() => {
        const getData = async () => {
            try {
                const data = {
                    id: `${localStorage.getItem('currentUserID')}`
                };
    
                const response = await fetch('http://localhost:4000/appointments/scheduleUser', {
                    method: 'POST',
                    credentials: 'include',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
                });
    
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
    
                const responseData = await response.json();
                console.log(responseData)
                setAppointments([...responseData.data]);
                console.log('stop');
            } catch (error) {
                console.error('Error fetching data:', error);
            }
                }
                    getData()
            }, [])

    
    const generateEndDate = (date: string) => {
        const newDate = new Date(date)
        newDate.setTime(newDate.getTime() + 3600000);

        const year = newDate.getUTCFullYear();
const month = String(newDate.getUTCMonth() + 1).padStart(2, '0');
const day = String(newDate.getUTCDate()).padStart(2, '0');
const hours = String(newDate.getUTCHours()).padStart(2, '0');
        const minutes = String(newDate.getUTCMinutes()).padStart(2, '0');
        
        const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:00.000Z`;
        return formattedDate
    }
    return (
        <Box>
            <NavBar></NavBar>
            <Scheduler
                view="month"
                hourFormat='24'
                resourceViewMode="tabs"
                editable={false}
                events={appointments.map(appointment => {
                    return {
                        event_id: appointment.id,
                        title: `${appointment.firstname} ${appointment.lastname}`,
                        start: new Date(appointment.dateofappointment),
                        end: new Date(generateEndDate(appointment.dateofappointment)),
                        draggable: false,
                        deletable: false,
                        editable:false,
                        note: appointment.note
                    }
                })}
                month={{
                    weekDays: [2, 3, 4, 5, 6],
                    weekStartOn: 6,
                    startHour: 9,
                    endHour: 17,
                    navigation: true,
                    disableGoToDay: false
                }}
                day={{
                    startHour: 9,
                    endHour: 17,
                    step: 60,
                    navigation: true
                }}
                viewerExtraComponent={(fields, event) => {
                    return (
                      <Box>
                        <Typography>Бележка: {event.note || "Няма..."}</Typography>
                      </Box>
                    );
                  }}
            />
      </Box>
    )
}
export default ScheduleUser