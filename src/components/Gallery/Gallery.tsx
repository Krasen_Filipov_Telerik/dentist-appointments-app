
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MobileStepper from '@mui/material/MobileStepper';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import { Container } from '@mui/material';
import NavBar from '../NavBar/NavBar.tsx';
import { useState } from 'react';
import first from '../../../public/ABC.jpg'
import second from '../../../public/ABC2.jpg'
import third from '../../../public/ABC3.jpg'
import fourth from '../../../public/ABC4.jpg'
import fifth from '../../../public/ABC5.jpg'
import './Gallery.css'

const images = [
  {
    label: 'Кореново лечение',
    imgPath:
    first,
    description:['Преди лечението', 'По време на лечението', 'След Лечението']
  },
  {
    label: 'Детска стоматология',
    imgPath: second,
    description: ['Преди лечението', 'След Лечението']
  },
  {
    label: 'Възстановяване на предни зъби',
    imgPath: third,
    description:['Преди лечението', 'По време на лечението', 'След Лечението']
  },
  {
    label: 'Възтановяване с металокерамична кострукция',
    imgPath: fourth,
    description:['Преди лечението', 'По време на лечението', 'След Лечението']
  },
  {
    label: 'Eстетично възстановяване на дъвкателни зъби',
    imgPath: fifth,
    description:['Преди лечението', 'След Лечението']
  },
];

const Gallery = () => {
  const theme = useTheme();
  const [activeStep, setActiveStep] = useState(0);
  const maxSteps = images.length;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };


  return (
    <Box>
      <NavBar></NavBar>
      <Box sx={{width:'100%'}}>
      <Paper
        square
        elevation={0}
        sx={{
          display: 'flex',
          height: 50,
          textAlign:'center',
          pl: 2,
          bgcolor: 'background.default',
        }}
      >
        </Paper>
        <Typography sx={{display:'flex',justifyContent: 'center',fontSize:'50px'}}>{images[activeStep].label}</Typography>
        <Container sx={{
          height:'450px',
          width:'100%',
          display:'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          minWidth:'100%'
        }}>
          <Box sx={{ display:'flex', flexDirection:'row',gap:'100px', justifyContent:'center'}}>
          <Box sx={{ fontSize:'25px'}}>
        {images[activeStep].description[0]}
            </Box>
            <Box sx={{fontSize:'25px'}}>
        {images[activeStep].description[1]}
            </Box>
            <Box sx={{ fontSize:'25px'}}>
        {images[activeStep].description[2]}
            </Box>
            </Box>
        <Box
        component="img"
        sx={{
          overflow: 'hidden',
          width:'50%',
          maxHeight: '400px',
          ml:'25%'
        }}
        src={images[activeStep].imgPath}
        alt={images[activeStep].label}
        
          />
          </Container>
      <MobileStepper
        variant="dots"
        steps={maxSteps}
        position="static"
        activeStep={activeStep}
        nextButton={
          <Button
            size="large"
            onClick={handleNext}
            disabled={activeStep === maxSteps - 1}
            sx={{
              '&:hover': {
                backgroundColor: '#2196f3',
                color: 'white',
              },
              color: '#2196f3',
              }}
          >
            Next
            {theme.direction === 'rtl' ? (
              <KeyboardArrowLeft />
            ) : (
              <KeyboardArrowRight />
            )}
          </Button>
        }
        backButton={
          <Button size="large" onClick={handleBack} disabled={activeStep === 0} sx={{
            '&:hover': {
              backgroundColor: '#2196f3',
              color: 'white',
            },
            color: '#2196f3',
            }}>
            {theme.direction === 'rtl' ? (
              <KeyboardArrowRight />
            ) : (
              <KeyboardArrowLeft />
            )}
            Back
          </Button>
        }
      />
    </Box>
      </Box>
  );
}

export default Gallery;

