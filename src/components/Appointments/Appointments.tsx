import dayjs from 'dayjs';
import * as React from 'react';
import { Dayjs } from 'dayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DateTimePicker } from '@mui/x-date-pickers';
import NavBar from '../NavBar/NavBar';
import { Button,Box, Container, Grid } from '@mui/material';
import { useState, useEffect } from 'react';
import InputMultiline from './textArea';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import LiveHelpIcon from '@mui/icons-material/LiveHelp';
import Tooltip, { TooltipProps, tooltipClasses } from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
interface Appointment {
    dateofappointment: string; 
  }
const Appointments = () => {
    const whatever = 'Tue Oct 24 2023 14:00:00 GMT+0300 (Eastern European Summer Time)'
    const [selectedFreshDate, setSelectedDate] = useState<Dayjs | null>(null);
    const [noteMessage, setNoteMessage] = useState<string>('')
    const [open, setOpen] = useState(false);
    const [alertStatus, setAlertStatus] = useState('error')
    const [appointments, setAppointments] = useState<Appointment[]>([]);
    const fiveAM = dayjs().set('hour', 16).startOf('hour');
    const nineAM = dayjs().set('hour', 9).startOf('hour');
    const lastMonday = dayjs().startOf('week');
const nextSunday = dayjs().endOf('week').startOf('day');
    const today = dayjs();
    
    useEffect(() => {
        const fetchAppointments = async() => {
            try {
                const response = await fetch('http://localhost:4000/appointments/create', {
                    credentials: 'include'
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                setAppointments([...data.data]);
                console.log(data);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchAppointments()
},[])
 
const isWeekend = (date: Dayjs) => {
    const day = date.day();
  
    return day === 0 || day === 6;
  };


    const handleDateChange = (newDate: Dayjs | null) => {
        setSelectedDate(newDate);
    };

    const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
        props,
        ref,
      ) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props}  />;
      });
    
    
    
      const handleClose = (_event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
      };



    const showDate = async() => {
        console.log(selectedFreshDate?.toDate())
        console.log(new Date(whatever))
        if (selectedFreshDate?.toDate().toLocaleTimeString() === new Date(whatever).toLocaleTimeString()) {
            console.log(true)
        }
        const data = {
            userId: localStorage.getItem("currentUserID"),
            date: selectedFreshDate?.toDate(),
            note: noteMessage,
            status: "pending"
}
       const bookAppointment = await (await fetch('http://localhost:4000/appointments/create', {
            method: 'POST',
            credentials: 'include',
            headers: {
              'Access-Control-Allow-Credentials': 'true',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
       })).json()
        console.log(bookAppointment)
        if (bookAppointment.status === 'success') {
            setAlertStatus('success')
            setOpen(true)
        } else if (bookAppointment.status === 'error') {
            setAlertStatus('error')
            setOpen(true)
        }
    }

    const HtmlTooltip = styled(({ className, ...props }: TooltipProps) => (
        <Tooltip {...props} classes={{ popper: className }} />
      ))(({ theme }) => ({
        [`& .${tooltipClasses.tooltip}`]: {
          backgroundColor: '#f5f5f9',
          color: 'rgba(0, 0, 0, 0.87)',
          maxWidth: 220,
          fontSize: theme.typography.pxToRem(12),
          border: '1px solid #dadde9',
        },
      }));

    return (
        <Box sx={{width:'100%'}}>
            <NavBar />
            <Snackbar anchorOrigin={{ vertical:"top", horizontal:"center" }} open={open} autoHideDuration={2000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={alertStatus === 'success' ? "success" : "error"} sx={{ width: '100%', fontSize:'26px'}}>
        {alertStatus === 'success' ? 'Успешно запазване на час!' : "Неуспешно запазване на час!"}
        </Alert>
      </Snackbar>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
                <Box sx={{mt:'10%'}}>
                    <Box sx={{ fontSize: '28px', justifyContent: 'center', display: 'flex' }}>Изберете дата и час за прегледа:
                    <HtmlTooltip
        title={
          <React.Fragment>
            <Typography color="inherit">Внимание:</Typography>
            <em>{"Някой часове"}</em> <b>{'може'}</b> <u>{'да не бъдат визуализирани,'}</u>.{' '}
            {"тъй като те са вече заети от други пациенти."}
          </React.Fragment>
        }
      >
        <Button><LiveHelpIcon/></Button>
      </HtmlTooltip>
                    </Box>
                    <Container sx={{display:'flex', justifyContent:'center'}}>
                        <DateTimePicker
                            minTime={nineAM}
                            maxTime={fiveAM}
                            skipDisabled={true}
                            minDate={today}
                            minutesStep={60}
                            sx={{width:"60%"}}
                            ampm={false}
                            views={["year", "month", "day", "hours"]}
                            format="dd MMMM YYYY HH:00"
                            value={selectedFreshDate}
                            onChange={handleDateChange}
                            slotProps={{
                                layout: {
                                  sx: {
                                    '.MuiDateCalendar-root': {
                                          width: '37rem',
                                          height:'29rem'
                                        },
                                        '.MuiPickersDay-root': {
                                            fontSize: "1.95rem",
                                            margin: '10px',
                                            width: '75px',
                                            height: '53px',
                                            borderRadius:'8px'
                                        },
                                        ".MuiMultiSectionDigitalClock-root": {
                                            width: "99px",
                                        },
                                        ".css-1e3wlyl-MuiButtonBase-root-MuiMenuItem-root-MuiMultiSectionDigitalClockSection-item": {
                                            fontSize: '29px',
                                            width:'60px'
                                        },
                                        ".css-rhmlg1-MuiTypography-root-MuiDayCalendar-weekDayLabel": {
                                            fontSize: '20px',
                                        },
                                        ".css-i5q14k-MuiDayCalendar-header": {
                                            gap:'45px'
                                        }
                                  }
                                }
                            }} 
                            shouldDisableDate={isWeekend}
                            shouldDisableTime={(value) =>
                                appointments.some((appointment) => {
                                  const appointmentDate = new Date(appointment.dateofappointment);
                                  return (
                                    value.isSame(appointmentDate, 'day') && value.hour() === appointmentDate.getHours()
                                  );
                                })
                              }
                        />
                    </Container>
                </Box>
                <Grid sx={{ mt: "5%", display: 'flex', flexDirection: "column", justifyContent: 'center', alignContent: 'center', height: 'max-height' }}>
                    <Box sx={{fontSize:'28px', justifyContent:'center', display:'flex'}}>Причина за прегледа:</Box>
                    <Box sx={{ display: 'flex', justifyContent: 'center' }}><InputMultiline setText={setNoteMessage}></InputMultiline></Box>
                    <Button onClick={showDate} size="large" sx={{
                        mt: "40px", fontSize: '24px', display: 'flex', alignSelf: 'center', width: '150px',
                       backgroundColor:'#2196f3', borderRadius:'8px', color:'white', ':hover':{backgroundColor:'#1976d2'}
                    }}>Запази</Button>
                    </Grid>
            </LocalizationProvider>
        </Box>
    );
}

export default Appointments;