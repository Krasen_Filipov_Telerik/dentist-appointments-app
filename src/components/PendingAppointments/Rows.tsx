import * as React from 'react';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { Button } from '@mui/material';
import Modal from '@mui/material/Modal';
interface Appointment {
    id: number;
    userid: string;
    firstname: string;
    lastname: string;
    egn: string;
    dateofappointment: string; 
    note: string;
    status: string;
  }

  interface RowProps {
      row: Appointment;
      setAppointments: React.Dispatch<React.SetStateAction<Appointment[]>>;
      appointments: Appointment[];
  }

 export default function Row(props: RowProps) {
     const { row } = props;
     const { appointments } = props;
     const { setAppointments } = props;
     const [open, setOpen] = React.useState(false);
     const [modalOpen, setModalOpen] = React.useState(false);
     const handleOpen = () => setModalOpen(true);
  const handleClose = () => setModalOpen(false);
  const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
     
     const handleApprove = async(appointmentId: number) => {
       const updateAppointments = await (await fetch('http://localhost:4000/appointments/pending', {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Access-Control-Allow-Credentials': 'true',
          'Content-Type': 'application/json',
        },
           body: JSON.stringify({id:appointmentId}),
       })).json()
         console.log(updateAppointments.status)
         const updateData = appointments.filter((a) => a.id !== appointmentId)
         console.log(updateData)
         setAppointments(updateData)
   }  
     
   const handleDecline = async(appointmentId: number) => {
    const updateAppointments = await (await fetch('http://localhost:4000/appointments/pending', {
     method: 'DELETE',
     credentials: 'include',
     headers: {
       'Access-Control-Allow-Credentials': 'true',
       'Content-Type': 'application/json',
     },
        body: JSON.stringify({id:appointmentId}),
    })).json()
      console.log(updateAppointments.status)
      const updateData = appointments.filter((a) => a.id !== appointmentId)
      console.log(updateData)
      setAppointments(updateData)
}      
     
     
  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.firstname} {row.lastname}
        </TableCell>
        <TableCell align="right">{row.egn}</TableCell>
        <TableCell align="right">{new Date(row.dateofappointment).toLocaleDateString()}</TableCell>
        <TableCell align="right">{new Date(row.dateofappointment).toLocaleTimeString()}</TableCell>
        <TableCell align="right">{row.userid}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant="h6" gutterBottom component="div">
                Бележка
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableRow>
                  <TableCell colSpan={6} sx={{borderBottom:'none', paddingLeft:'0px'}}>
                {row.note}
                  </TableCell>
                </TableRow>
                          </Table>
                          <Button size='small' onClick={() => handleApprove(row.id)} sx={{ color: 'white', backgroundColor: '#2196f3', ':hover': { backgroundColor: '#1976d2' } }}>Приеми часа</Button>
                          <Button size='small' onClick={handleOpen} sx={{ color: 'white', backgroundColor: '#2196f3', ml: '10px', ':hover': { backgroundColor: '#1976d2' } }}>Откажи</Button>
              <Box>
                <Modal
                  open={modalOpen}
                  onClose={handleClose}
                  aria-labelledby="modal-modal-title"
                  aria-describedby="modal-modal-description"
                >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Отказване на заявка
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            Сигурни ли сте, че искате да изтриете заявката? Промените не могат дат бъдат върнати!
                                      </Typography>
                                      <Box sx={{display:'flex', justifyContent:'flex-end'}}>
                                  <Button size="large" onClick={() => handleDecline(row.id)} sx={{ color: 'white', backgroundColor: '#f44336', ':hover': { backgroundColor: '#b71c1c' } }}>Да</Button>
                                          <Button size="large" onClick={handleClose} sx={{ color: 'white',ml:'10px', backgroundColor: '#bdbdbd', ':hover': { backgroundColor: '#e0e0e0' } }}>Не</Button>
                                          </Box>
                                  </Box>
        </Modal>
              </Box>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

{/* <TableHead>
                  <TableRow>
                    <TableCell>Date</TableCell>
                    <TableCell>Customer</TableCell>
                    <TableCell align="right">Amount</TableCell>
                    <TableCell align="right">Total price ($)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.history.map((historyRow) => (
                    <TableRow key={historyRow.date}>
                      <TableCell component="th" scope="row">
                        {historyRow.date}
                      </TableCell>
                      <TableCell>{historyRow.customerId}</TableCell>
                      <TableCell align="right">{historyRow.amount}</TableCell>
                      <TableCell align="right">
                        {Math.round(historyRow.amount * row.price * 100) / 100}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody> */}