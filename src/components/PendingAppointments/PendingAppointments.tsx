import React, { useEffect, useState } from 'react';
import { Box } from '@mui/material';
import NavBar from '../NavBar/NavBar';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Row from './Rows';
interface Appointment {
    id: number;
    userid: string;
    firstname: string;
    lastname: string;
    egn: string;
    dateofappointment: string; 
    note: string;
    status: string;
  }
const PendingAppointments = () => {
    const [appointments, setAppointments] = useState<Appointment[]>([]);
    
    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch('http://localhost:4000/appointments/pending', {
                    credentials: 'include'
                });
    
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
    
                const data = await response.json();
                setAppointments([...data.data]);
                console.log(appointments);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
    
        fetchData();
    }, []);

    return (
        <Box>
            <NavBar></NavBar>
            <TableContainer component={Paper} sx={{mt:'50px' }}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Име и Фамилия</TableCell>
            <TableCell align="right">ЕГН</TableCell>
            <TableCell align="right">Запазена дата</TableCell>
            <TableCell align="right">Час</TableCell>
            <TableCell align="right">ID</TableCell>
          </TableRow>
        </TableHead>
                    {appointments && <TableBody>
                        {appointments.map((row) => (
                            <Row key={row.id} row={row} setAppointments={setAppointments} appointments={appointments} />
                        ))}
                    </TableBody>}
      </Table>
    </TableContainer>
        </Box>
    );
}

export default PendingAppointments;