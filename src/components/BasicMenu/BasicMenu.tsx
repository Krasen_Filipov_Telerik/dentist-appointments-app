import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import LoginIcon from '@mui/icons-material/Login';
import { NavLink } from 'react-router-dom';
import { useState } from 'react';
const BasicMenu = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  


  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const logOut = () => {
    localStorage.clear();
  };

  return (
    <div>
      <Button
        id='basic-button'
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup='true'
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        size='large'
      >
        <LoginIcon sx={{ color: 'white', fontSize: '40px' }}></LoginIcon>
      </Button>
      <Menu
        id='basic-menu'
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        {!localStorage.getItem("currentUserStatus") && (
          <NavLink to='/auth/login'>
            <MenuItem onClick={handleClose}>Влез</MenuItem>
          </NavLink>
        )}
        { !localStorage.getItem("currentUserStatus") && (
          <NavLink to='/auth/register'>
            <MenuItem onClick={handleClose}>Регистрация</MenuItem>
          </NavLink>
        )}
            {localStorage.getItem("currentUserStatus") &&(
              <NavLink to='/'>
                <MenuItem onClick={logOut}>Здравей, {localStorage.getItem("currentUserName")}</MenuItem>
              </NavLink>
            )}
        { localStorage.getItem("currentUserStatus") && (
          <NavLink to='/'>
            <MenuItem onClick={logOut}>Изход</MenuItem>
          </NavLink>
        )}
      </Menu>
    </div>
  );
};
export default BasicMenu;
