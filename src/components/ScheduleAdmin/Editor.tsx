import dayjs from 'dayjs';
import * as React from 'react';
import { Dayjs } from 'dayjs';
import { useState, useEffect } from "react";
import { TextField, Button, DialogActions } from "@mui/material";
import { Box, Container, Grid } from '@mui/material';
import { DateTimePicker } from '@mui/x-date-pickers';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';
import type {
  ProcessedEvent,
  SchedulerHelpers
} from "@aldabil/react-scheduler/types";

interface CustomEditorProps {
  scheduler: SchedulerHelpers;
}
interface Appointment {
  dateofappointment: string;
  id: number;
}

const CustomEditor = ({ scheduler }: CustomEditorProps) => {
  const event = scheduler.edited;
  const today = dayjs();
  // Make your own form/state
  const [state, setState] = useState({
    title: event?.title || "",
      description: event?.description || "",
    egn: event?.egn || ""
  });
    const [selectedFreshDate, setSelectedDate] = useState<Dayjs | null>(null);
    const [appointments, setAppointments] = useState<Appointment[]>([]);
    const [open, setOpen] = useState(false);
  const [error, setError] = useState("");
  const fiveAM = dayjs().set('hour', 16).startOf('hour');
  const nineAM = dayjs().set('hour', 9).startOf('hour');
  const lastMonday = dayjs().startOf('week');
    const nextSunday = dayjs().endOf('week').startOf('day');
    const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
        props,
        ref,
      ) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props}  />;
      });
    useEffect(() => {
        const fetchAppointments = async() => {
            try {
                const response = await fetch('http://localhost:4000/appointments/scheduleAdmin', {
                    credentials: 'include'
                });
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                const data = await response.json();
                setAppointments([...data.data]);
                console.log(data);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
        fetchAppointments()
},[])



    const handleChange = (value: string, name: string) => {
    setState((prev) => {
      return {
        ...prev,
        [name]: value
      };
    });
  };
    const handleSubmit = async () => {
        const typeOfAction = event ? "edit" : "create"
      const givenPatientName = state.title.split(" ")
      let ifCreate = 0;
    if (state.title.trim().length < 3) {
      return setError("Твърде кратко име и фамилия!");
        }
        if (givenPatientName.length < 2 || givenPatientName.length > 2) {
            return setError("Невалидни имена на пациент!");
        }
        if (!selectedFreshDate) {
            return setError("Моля изберете дата!");
        }
        if (!(state.egn.trim().length === 10)) {
            return setError("Невалидно ЕГН!");
        }
        if (isNaN(+state.egn)) {
            return setError("Невалидно ЕГН!");
        }
        if (new Date() > new Date(`${selectedFreshDate}`)) {
            return setError("Невалидна дата и час!");
        }
        try {
        
            if (typeOfAction === "edit") {
                console.log(event?.event_id)
                const data = {
                    userId: localStorage.getItem("currentUserID"),
                    firstName: givenPatientName[0],
                    lastName: givenPatientName[1],
                    dateofappointment: selectedFreshDate?.toDate(),
                    note: state?.description,
                    status: "approved",
                    id: event?.event_id
                    
        }
                const editAppointment = await (await fetch('http://localhost:4000/appointments/scheduleAdmin', {
                    method: 'PUT',
                    credentials: 'include',
                    headers: {
                      'Access-Control-Allow-Credentials': 'true',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
               })).json()  
            } else if (typeOfAction === "create") {
                const data = {
                    userId: localStorage.getItem("currentUserID"),
                    firstName: state.title.split(' ')[0],
                    lastName: state.title.split(' ')[1],
                    egn: state.egn,
                    dateofappointment: selectedFreshDate?.toDate(),
                    note: state?.description,
                    status: "approved"
        }
                const editAppointment = await (await fetch('http://localhost:4000/appointments/scheduleAdmin', {
                    method: 'POST',
                    credentials: 'include',
                    headers: {
                      'Access-Control-Allow-Credentials': 'true',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(data),
                })).json() 
              ifCreate = editAppointment.id
            }



      scheduler.loading(true);
      const added_updated_event = (await new Promise((res) => {
          setTimeout(() => {
          res({
            event_id: event?.event_id || +ifCreate,
            title: state.title,
            start: new Date(`${selectedFreshDate}`),
            end: new Date(generateEndDate(`${selectedFreshDate}`)),
              description: state.description,
          });
        }, 1000);
      })) as ProcessedEvent;
        scheduler.onConfirm(added_updated_event, typeOfAction);
      scheduler.close();
    } finally {
      scheduler.loading(false);
    }
    };
    const isWeekend = (date: Dayjs) => {
        const day = date.day();
      
        return day === 0 || day === 6;
      };

      const handleDateChange = (newDate: Dayjs | null) => {
        setSelectedDate(newDate);
    };
 
    const generateEndDate = (date: string) => {
        const newDate = new Date(date)
        newDate.setTime(newDate.getTime() + 3600000);

        const year = newDate.getUTCFullYear();
const month = String(newDate.getUTCMonth() + 1).padStart(2, '0');
const day = String(newDate.getUTCDate()).padStart(2, '0');
const hours = String(newDate.getUTCHours()).padStart(2, '0');
        const minutes = String(newDate.getUTCMinutes()).padStart(2, '0');
        
        const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:00.000Z`;
        return formattedDate
    }   
    
    const handleClose = (_event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
      };

  return (
    <div>
          <div style={{ padding: "1rem" }}>
          {error && <Snackbar anchorOrigin={{ vertical:"top", horizontal:"center" }} open={open} autoHideDuration={2000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={"error"} sx={{ width: '100%', fontSize:'26px'}}>
        {error}
        </Alert>
      </Snackbar>}
              <Box sx={{fontSize:'35px', textAlign:'center'}}>Нов час:</Box>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
              <Container sx={{display:'flex', justifyContent:'center'}}>
                        <DateTimePicker
                            minTime={nineAM}
                            maxTime={fiveAM}
                            skipDisabled={true}
                            minDate={today}
                           minutesStep={60}
                          sx={{ width: "60%" }}
                          disablePast={true}
                            ampm={false}
                            views={["year", "month", "day", "hours"]}
                            format="dd MMMM YYYY HH:00"
                            value={selectedFreshDate}
              onChange={handleDateChange}
                            slotProps={{
                                layout: {
                                  sx: {
                                    '.MuiDateCalendar-root': {
                                          width: '37rem',
                                          height:'29rem'
                                        },
                                        '.MuiPickersDay-root': {
                                            fontSize: "1.95rem",
                                            margin: '10px',
                                            width: '75px',
                                            height: '53px',
                                            borderRadius:'8px'
                                        },
                                        ".MuiMultiSectionDigitalClock-root": {
                                            width: "99px",
                                        },
                                        ".css-1e3wlyl-MuiButtonBase-root-MuiMenuItem-root-MuiMultiSectionDigitalClockSection-item": {
                                            fontSize: '29px',
                                            width:'60px'
                                        },
                                        ".css-rhmlg1-MuiTypography-root-MuiDayCalendar-weekDayLabel": {
                                            fontSize: '20px',
                                        },
                                        ".css-i5q14k-MuiDayCalendar-header": {
                                            gap:'45px'
                                        }
                                  }
                                }
                            }} 
                            shouldDisableDate={isWeekend}
                            shouldDisableTime={(value) =>
                                appointments.some((appointment) => {
                                  const appointmentDate = new Date(appointment.dateofappointment);
                                  return (
                                    value.isSame(appointmentDate, 'day') && value.hour() === appointmentDate.getHours()
                                  );
                                })
                              }
                        />
                    </Container>
                  <TextField
                      required={true}
          label="Име и Фамилия"
          value={state.title}
          onChange={(e) => handleChange(e.target.value, "title")}
          error={!!error}
          helperText={error}
                      fullWidth
                      sx={{mt:'20px'}}
        />
                  <TextField
                      required={true}
          label="Бележка"
          value={state.description}
          onChange={(e) => handleChange(e.target.value, "description")}
                      fullWidth
                      sx={{mt:'20px'}}
                  />
                  <TextField
                      required={true}
          label="ЕГН"
          value={state.egn}
          onChange={(e) => handleChange(e.target.value, "egn")}
                      fullWidth
                      sx={{mt:'20px'}}
        />
          </LocalizationProvider>
          </div>
      <DialogActions>
        <Button onClick={scheduler.close}>Откажи</Button>
        <Button onClick={handleSubmit}>Потвърди</Button>
          </DialogActions>
    </div>
  );
};
export default CustomEditor