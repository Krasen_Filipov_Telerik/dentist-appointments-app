import * as React from 'react';
import NavBar from '../NavBar/NavBar'
import { Box, Typography } from '@mui/material'
import { Scheduler } from "@aldabil/react-scheduler";
import { useState, useEffect } from 'react';
import CustomEditor from './Editor.tsx';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
interface Appointment {
    id: number;
    userid: string;
    firstname: string;
    lastname: string;
    egn: string;
    dateofappointment: string; 
    note: string;
    status: string;
}
const ScheduleAdmin = () => {
    const [appointments, setAppointments] = useState<Appointment[]>([])
    const [deleteStatus, setDeleteStatus] = useState("")
    const [open, setOpen] = useState(false);
    const handleClose = (_event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };
    
    const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
        props,
        ref,
      ) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props}  />;
      });
    useEffect(() => {
        const getData = async () => {
            try {
                const response = await fetch('http://localhost:4000/appointments/scheduleAdmin', {
                    credentials: 'include'
                });
    
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
    
                const data = await response.json();
                setAppointments([...data.data]);
                console.log('stop')
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }
            getData()
        }, [])
        
    const generateEndDate = (date: string) => {
        const newDate = new Date(date)
        newDate.setTime(newDate.getTime() + 3600000);

        const year = newDate.getUTCFullYear();
const month = String(newDate.getUTCMonth() + 1).padStart(2, '0');
const day = String(newDate.getUTCDate()).padStart(2, '0');
const hours = String(newDate.getUTCHours()).padStart(2, '0');
        const minutes = String(newDate.getUTCMinutes()).padStart(2, '0');
        
        const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:00.000Z`;
        return formattedDate
    }

    const handleDelete = async (deletedId: string): Promise<string> => {
        console.log(deletedId)
        const removeAppointment = await (await fetch('http://localhost:4000/appointments/scheduleAdmin', {
            method: 'DELETE',
            credentials: 'include',
            headers: {
                'Access-Control-Allow-Credentials': 'true',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({id: deletedId})
        })).json()
        if (removeAppointment.status === "error") {
            setDeleteStatus("error")
            setOpen(true)
        } else if (removeAppointment.status === "success") {
            setDeleteStatus("success")
            setOpen(true)
        }
        return new Promise((res, rej) => {
          setTimeout(() => {
              res(deletedId);
          },1000);
        });
      };


    return (
        <Box>
            <NavBar></NavBar>
            <Snackbar anchorOrigin={{ vertical:"top", horizontal:"center" }} open={open} autoHideDuration={2000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={deleteStatus === 'success' ? "success" : "error"} sx={{ width: '100%', fontSize:'26px'}}>
        {deleteStatus === 'success' ? 'Успешно изтрихте този час!' : "Неуспешно изтриване на часа!"}
        </Alert>
      </Snackbar>
            <Scheduler
                view="month"
                hourFormat='24'
                resourceViewMode="tabs"
                events={appointments.map(appointment => {
                    return {
                        event_id: appointment.id,
                        title: `${appointment.firstname} ${appointment.lastname}`,
                        start: new Date(appointment.dateofappointment),
                        end: new Date(generateEndDate(appointment.dateofappointment)),
                        draggable: false,
                        deletable: true,
                        note: appointment.note
                    }
                })}
                month={{
                    weekDays: [2, 3, 4, 5, 6],
                    weekStartOn: 6,
                    startHour: 9,
                    endHour: 17,
                    navigation: true,
                    disableGoToDay: false
                }}
                day={{
                    startHour: 9,
                    endHour: 17,
                    step: 60,
                    navigation: true
                }}
                onDelete={handleDelete}
                customEditor={(scheduler) => <CustomEditor scheduler={scheduler} />}
                viewerExtraComponent={(fields, event) => {
                    return (
                      <Box>
                        <Typography>Бележка: {event.note || "Няма..."}</Typography>
                      </Box>
                    );
                  }}
            />
            
        </Box>
    )
}

export default ScheduleAdmin