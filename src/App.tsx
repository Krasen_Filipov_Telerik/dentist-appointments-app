import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from 'react-router-dom';
import Gallery from './components/Gallery/Gallery.tsx';
import Home from './components/Home/Home.tsx';
import Contacts from './components/Contacts/Contacts.tsx';
import Facts from './components/Facts/Facts.tsx';
import Login from './components/Login/Login.tsx';
import Register from './components/Register/Register.tsx';
import AppInitializers from './AppInitializers.tsx'
import Appointments from './components/Appointments/Appointments.tsx';
import PendingAppointments from './components/PendingAppointments/PendingAppointments.tsx';
import ScheduleAdmin from './components/ScheduleAdmin/ScheduleAdmin.tsx';
import ScheduleUser from './components/ScheduleUser/ScheduleUser.tsx';
import Footer from './components/Footer/Footer.tsx';
const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path='/' element={<Home />}></Route>
      <Route path='/gallery' element={<Gallery />} />
      <Route path='/contacts' element={<Contacts />} />
      <Route path='/facts' element={<Facts />} />
      <Route path='/auth/login' element={<Login />} />
      <Route path='/auth/register' element={<Register />} />
      <Route path='/appointments/create' element={<Appointments />} />
      <Route path='/appointments/pending' element={<PendingAppointments />} />
      <Route path='/appointments/scheduleAdmin' element={<ScheduleAdmin />} />
      <Route path='/appointments/scheduleUser' element={<ScheduleUser />} />
    </>
  )
);

function App() {
  return (
    <AppInitializers>
      <RouterProvider router={router} />
      <Footer></Footer>
    </AppInitializers>
  );
}

export default App;
