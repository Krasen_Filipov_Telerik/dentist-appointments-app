import { useContext, useEffect, useState } from 'react';
import { AppContext } from './AppContext';
import { ReactNode } from 'react';
interface UserData {
  loggedIn: boolean;
  email: string;
  firstName: string;
  lastName: string;
  egn: string;
}

interface AppInitializersProps {
  children: ReactNode;
}

const AppInitializers: React.FC<AppInitializersProps> = ({ children }) => {
  const [userData, setUserData] = useState<UserData>({
    loggedIn: false,
    email: '',
    firstName: '',
    lastName: '',
    egn: '',
  });

  useEffect(() => {
    fetch('http://localhost:4000/auth/login', {
      credentials: 'include',
    })
      .then(async (res) => {
        if (!res || !res.ok || res.status >= 400) {
          throw new Error('Login failed');
        }
        const data = await res.json();
        setUserData(data);
      })
      .catch((err) => {
        console.error('Error:', err);
        setUserData({
          loggedIn: false,
          email: '',
          firstName: '',
          lastName: '',
          egn: '',
        });
      });
    const userID = `${localStorage.getItem("currentUserID")}`
    const adminList = import.meta.env.VITE_REACT_APP_ADMIN_LIST;
    if (adminList.split(',').includes(userID)) {
      localStorage.setItem("isAdmin", 'true')
    } else {
      localStorage.setItem("isAdmin", 'false')
    }
  }, []);

  return (
    <AppContext.Provider value={{ userData, setUserData }}>
      {children}
    </AppContext.Provider>
  );
};

export default AppInitializers;

export const useUserContext = () => {
  return useContext(AppContext);
};
