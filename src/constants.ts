export const MIN_NAME_LENGTH:number = 2;
export const MAX_NAME_LENGTH:number = 20;
export const MIN_PASSWORD_LENGTH:number = 6;
export const MAX_PASSWORD_LENGTH:number = 30;
export const MIN_LAST_NAME_LENGTH:number = 4;
export const MAX_LAST_NAME_LENGTH:number = 20;
export const MIN_EGN_LENGTH:number = 10;