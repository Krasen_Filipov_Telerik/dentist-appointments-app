const express = require("express");
const router = express.Router();
const pool = require("./src/db.cjs")
const bcrypt = require("bcrypt")

router.route("/login")
    .get(async (req, res) => {
        if (req.session.user && req.session.user.username) {
            console.log("logged in")
            res.json({loggedIn: true, username: req.session.user.username, firstName: req.session.user.firstName, lastName: req.session.user.lastName, egn: req.session.user.egn})
        } else {
            res.json({loggedIn:false})
        }
    })
    .post( async (req, res) => {
    const potentialLogin = await pool.query("SELECT id, email, passhash, firstname, lastname, egn FROM users WHERE email=$1",
    [req.body.email]
    )
    if (potentialLogin.rowCount > 0) {
        const isSamePass = await bcrypt.compare(req.body.password, potentialLogin.rows[0].passhash)
        if (isSamePass) {
            req.session.user = {
                username: req.body.email,
                id: potentialLogin.rows[0].id,
                firstName: potentialLogin.rows[0].firstname,
                lastName: potentialLogin.rows[0].lastname,
                egn: potentialLogin.rows[0].egn

            }
            const userData = {
                id: potentialLogin.rows[0].id,
                firstName: potentialLogin.rows[0].firstname,
                loggedIn: true
        }
        res.json({ userInfo: userData })
        } else {
            res.json({ loggedIn: false, status: "Wrong username or password" })
        }
    } else {
        res.json({loggedIn:false, status: "Wrong username or password"})
        }   
});

router.post("/register", async(req, res) => {
    
    const existingUser = await pool.query("SELECT egn from public.users WHERE egn=$1 AND email=$2",
        [req.body.egn, req.body.email]
    );
    if (existingUser.rowCount === 0) {
        const saltRounds = 10
        const salt = await bcrypt.genSalt(saltRounds)
        const hashedPass = await bcrypt.hash(req.body.password, salt)
        const newUserQuery = await pool.query("INSERT INTO users(email, passhash, egn, firstName, lastName) values($1,$2,$3,$4,$5) RETURNING id,email,firstName,lastName,egn",
            [req.body.email, hashedPass, req.body.egn, req.body.firstName, req.body.lastName]
        )
        req.session.user = {
            email: req.body.email,
            id: newUserQuery.rows[0].id
        }
        const userData = {
            id: newUserQuery.rows[0].id,
                firstName: newUserQuery.rows[0].firstName,
                loggedIn: true
        }
        res.json({ userInfo: userData })
    } else {
        res.json({ loggedIn: false, status: 'EGN taken' })
}
});

module.exports = router;